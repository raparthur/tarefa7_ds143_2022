#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
 int info;
 struct arvore *esq;
 struct arvore *dir;
} Arvore;


int verifica_arv_vazia (Arvore* a) {
 return (a == NULL);
}

Arvore* cria_arv_vazia (void) {
 return NULL;
}

Arvore* arv_constroi (int c, Arvore* e, Arvore* d) {
 Arvore* a = (Arvore*)malloc(sizeof(Arvore));
 a->info = c;
 a->esq = e;
 a->dir = d;
 return a;
}

void pre_ordem(Arvore* a)
{
  if(!verifica_arv_vazia(a))
  {
    printf("%d,", a->info);
    pre_ordem(a->esq);
    pre_ordem(a->dir);
  }
}

void in_ordem(Arvore * a){
   if(!verifica_arv_vazia(a))
  {
    in_ordem(a->esq);
    printf("%d,", a->info);
    in_ordem(a->dir);
  }
}

void pos_ordem(Arvore * a){
   if(!verifica_arv_vazia(a))
  {
    pos_ordem(a->esq);
    pos_ordem(a->dir);
    printf("%d,", a->info);
  }
}

int buscar(Arvore *a, int v){
    if(a == NULL) {return 0;}
    else if(v < a->info){
        return buscar(a->esq, v);
    }
    else if(v > a->info){
        return buscar(a->dir, v);
    }
    else {return 1;}
}

Arvore* inserir(Arvore* a, int v){
    if(a == NULL){
        a = (Arvore*)malloc(sizeof(Arvore));
        a->info= v;
        a->esq = a->dir = NULL;
    }
    else if(v < a->info){
        a->esq = inserir(a->esq, v);
    }
    else{
        a->dir = inserir(a->dir, v);
    }
    return a;
}

int random(int min, int max){
   return min + rand() / (RAND_MAX / (max - min + 1) + 1);
}
//arvore de busca com chaves n�o repetidas
Arvore * cria_arvore_aleatoria(int tam,int range){
    Arvore* a = cria_arv_vazia ();
    int Array[tam], i;
    while (i != tam) {
        int candidate = ((rand() % range) + 1);
        int ok = 1; // ok will become 0 if a duplicate is found
        for (int j = 0 ; ok && j != i ; j++) {
            ok &= (candidate != Array[j]);
        }
        if (ok) {
            Array[i++] = candidate;
            a = inserir(a,candidate);
        }
    }
    return a;
}

int encontra_max(Arvore * a){
    if(a == NULL){
        return -1;
    }
    if(a->dir == NULL){
        return a->info;
    }
    return encontra_max(a->dir);
}

int encontra_min(Arvore * a){
    if(a == NULL){
        return -1;
    }
    if(a->esq == NULL){
        return a->info;
    }
    return encontra_min(a->esq);
}
/*
//minha versao
Arvore * remover(Arvore * a, int valor){

    if(a == NULL){
        return NULL;
    } else if(valor < a->info){
        remover(a->esq, valor);
    } else if(valor > a->info){
        remover(a->dir, valor);
    } else {
        //caso 1
        if(a->dir == NULL && a->esq == NULL){
            a = NULL;
            free(a);
        } else if(a->dir == NULL && a->esq != NULL){
            a = a->esq;
            free(a->esq);
        } else if(a->dir != NULL && a->esq == NULL){
            a = a->dir;
            free(a->dir);
        } else {
            while(a->esq != NULL){
                a->esq = a->esq->dir;
            }
            a->esq->info = valor;
            a->dir = a->esq->dir;
            free(a->esq);
            return a;
        }

    }
}
*/

Arvore* remover_cola(Arvore *a, int valor){

  if(a == NULL){
        return NULL;
  }

  if(valor < a->info){
        a->esq = remover_cola(a->esq, valor);
    } else if(valor > a->info){
        a->dir = remover_cola(a->dir, valor);
    } else {

        //caso 1
        if(a->dir == NULL && a->esq == NULL){
            a = NULL;
            free(a);
        //caso 2
        } else if(a->dir == NULL && a->esq != NULL){
            a = a->esq;
            free(a->esq);
        } else if(a->dir != NULL && a->esq == NULL){
            a = a->dir;
            free(a->dir);
        //caso 3
        } else {
            Arvore * aux = a->esq;
            Arvore * pai_aux = a;
            while(aux->dir)
            {
                pai_aux = aux;
                aux = aux->dir;
            }
            int tmp = a->info;
            a->info = aux->info;
            aux->info = tmp;
            pai_aux->dir = remover_cola(aux,tmp);
            return(a);
        }
    }

  return(a);
}
//quase consegui fazer sozinho, precisei da internet pra me dar o toque que preciso ver se todos os valores de um lado s�o maiores/menores que o procurado
//referencia: https://www.geeksforgeeks.org/a-program-to-check-if-a-binary-tree-is-bst-or-not/
int arv_bin_check(Arvore * a){

    if(verifica_arv_vazia(a)){
        //arvore vazia definida como binaria
        return 1;

    }
    if(a->esq != NULL){
        if(encontra_max(a->esq) < a->info){
            //todos os valores da esquerda s�o maiores que o procurando, continua procurando
            a->esq = arv_bin_check(a->esq);
        } else{
            //tem algum valor maior que a o da raiz na esquerda, n�o � bin�ria
            return 0;
        }
    }
    if(a->dir != NULL){
        if(encontra_min(a->dir) > a->info){
            //todos os valores da direita s�o maiores que o procurando, continua procurando
            a->dir = arv_bin_check(a->dir);
        } else {
            return 0;
        }
    }
    //passou em todas contraprovas
    return 1;
}

int main()
{
    Arvore* a = cria_arv_vazia ();
    //exerc 2
    //int i;
    /*
    for (i = 0; i < 999; i++)
    {
        int num = random(0,999);
        a = inserir(a,num);
    }
    printf("--------- 0-999 ----------\n");
    in_ordem(a);

    //exerc 3
    for (i = 0; i < 100000; i++)
    {
        int num = random(0,99999);
        a = inserir(a,num);
    }
    printf("--------- 0-99999 ----------\n");
    printf("achou em aleatorio:%d\n",buscar(a,100000));

    //exerc 3
    for (i = 0; i < 40000; i++)
    {
        a = inserir(a,i);
    }
    printf("--------- 0-99999 ----------\n");
    printf("achou em ordenado:%d\n",buscar(a,100000));


    //arvore com chaves nao repetidas

    a = cria_arvore_aleatoria(10,50);
    printf("\n---------busca in ordem da arvore:-------------\n");
    pre_ordem(a);
    printf("\n------------ maior chave:%d-----------\n",encontra_max(a));
    printf("\n------------ menor chave:%d-----------\n",encontra_min(a));

     Arvore* b;
     b = inserir(NULL,4);
     b = inserir(b,6);
     b = inserir(b,3);
     b = inserir(b,5);
     b = inserir(b,1);
     b = inserir(b,7);
     b = inserir(b,2);
     b = inserir(b,8);

   printf("--------- arvore balanceada binaria ----------\n");
   in_ordem(b);
   printf("\n--------- remove 5 (folha) ----------\n");
   b = remover_cola(b,5);
   in_ordem(b);
   printf("\n--------- remove 7 (1 filho) ----------\n");
   b = remover_cola(b,7);
   in_ordem(b);
   printf("\n--------- remove 3 (2 filhos) ----------\n");
   b = remover_cola(b,3);
   in_ordem(b);

   */

   printf("\n############ TRABALHO ##################\n");

   Arvore* c;
     c = inserir(NULL,4);
     c = inserir(c,6);
     c = inserir(c,3);
     c = inserir(c,5);
     c = inserir(c,1);
     c = inserir(c,7);
     c = inserir(c,2);
     c = inserir(c,8);

   printf("\n--------- arvore C ----------\n");
   in_ordem(c);
   printf("\n--------- C eh binaria: %d----------\n",arv_bin_check(c));


   Arvore *d = arv_constroi (1,
            arv_constroi(2,
               cria_arv_vazia(),
               arv_constroi(4,cria_arv_vazia(),cria_arv_vazia())
            ),
            arv_constroi(3,
               arv_constroi(5,cria_arv_vazia(),cria_arv_vazia()),
               arv_constroi(7,cria_arv_vazia(),
                            arv_constroi(6,cria_arv_vazia(),cria_arv_vazia()))
            )
   );

   printf("\n--------- arvore D ----------\n");
   in_ordem(d);
   printf("\n--------- D eh binaria: %d----------\n",arv_bin_check(d));

    return 0;
}
